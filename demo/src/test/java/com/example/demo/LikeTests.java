package com.example.demo;

import com.example.demo.controller.common.ResultVO;
import com.example.demo.controller.vo.LikeVO;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class LikeTests {

    @Test
    public void moreLike() {
        for (int i = 0, len = 10000; i< len; i++) {
            ResultVO vo = RestClient.create().put()
                    .uri("http://127.0.0.1:9999/like")
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Map.of("userId", "test" + i, "contentId", "test001"))
                    .exchange((clientRequest, clientResponse) -> clientResponse.bodyTo(ResultVO.class));
            System.out.println("vo = " + vo);
        }


    }


    private Integer corePoolSize = 10;
    private Integer maximumPoolSize = 20;
    private Long keepAliveTime = 60L;
    public ExecutorService executorService() {
        return new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>());
    }
    @Test
    public void moreLike2() {
        ExecutorService executorService = executorService();
        List<CompletableFuture> list= new ArrayList<>();
        for (int i = 0, len = 10000; i< len; i++) {
            final int a = i;
            list.add(CompletableFuture.supplyAsync(()-> {
                ResultVO vo = RestClient.create().put()
                        .uri("http://127.0.0.1:9999/like")
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(Map.of("userId", "test" + a, "contentId", "test012"))
                        .exchange((clientRequest, clientResponse) -> clientResponse.bodyTo(ResultVO.class));
                System.out.println("vo = " + vo);
                return vo;
            }, executorService));
        }
        CompletableFuture[] arr = new CompletableFuture[list.size()];
        for (int i = 0, len = list.size(); i < len; i++) {
            arr[i] = list.get(i);
        }
        try {
            CompletableFuture.allOf(arr).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
