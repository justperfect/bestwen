package com.example.demo;

import com.example.demo.tree.TreeNode;
import com.example.demo.tree.TreeTool;
import com.sun.source.tree.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TreeToolTests {
    public static void main(String[] args) {
        List<TreeNode> list = new ArrayList<>();
        list.add(new TreeNode() {{setTid("root");setCode("root");setName("根节点");setPid("");}});
        list.add(new TreeNode() {{setTid("a");setCode("a");setName("a节点");setPid("root");}});
        list.add(new TreeNode() {{setTid("a1");setCode("a1");setName("a1节点");setPid("a");}});
        list.add(new TreeNode() {{setTid("a2");setCode("a2");setName("a2节点");setPid("a");}});
        list.add(new TreeNode() {{setTid("a3");setCode("a3");setName("a3节点");setPid("a");}});
        list.add(new TreeNode() {{setTid("b");setCode("b");setName("b节点");setPid("root");}});
        list.add(new TreeNode() {{setTid("c");setCode("c");setName("c节点");setPid("b");}});
        list.add(new TreeNode() {{setTid("c1");setCode("c1");setName("c1节点");setPid("c");}});
        list.add(new TreeNode() {{setTid("c2");setCode("c2");setName("c2节点");setPid("c");}});
        TreeTool treeTool = new TreeTool();
        List<TreeNode> root = treeTool.tree(list, "root");
        System.out.println("root = " + root);
        List<TreeNode> result = treeTool.loop(root);
        System.out.println("result = " + result);
        List<TreeNode> loop = root.get(0).getChilren().get(0).loop();
        System.out.println("loop = " + loop);
    }
}
