package com.example.demo;

import com.example.demo.redis.RedisTool;
import com.example.demo.tree.TreeNode;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

    @Test
    void contextLoads() {
    }

    @Resource
    private RedisTool redisTool;
    @Test
    public void set() {
        redisTool.set("test:baba", "here is my test demo!");
    }
    @Test
    public void setTreeNode() {//必须要可序列化的模型
        redisTool.set("test:baba", new TreeNode());
    }
    @Test
    public void getRedis() {
        Object o = redisTool.get("test:baba");
        System.out.println("o = " + o);
    }
}
