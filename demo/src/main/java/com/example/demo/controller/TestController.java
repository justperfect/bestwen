package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/demo1")
    public Map demo1() {
        return Map.of("demo1", "demo1-value");
    }
    @RequestMapping("/demo2")
    public Map demo2() {
        return Map.of("demo2", "demo2-value");
    }
    @RequestMapping("/demo3")
    public Map demo3() {
        return Map.of("demo3", "demo3-value");
    }
}
