package com.example.demo.controller.common;

public class ResultVO<T> {
    private Integer code;
    private Boolean success;
    private String msg;
    private T data;

    public static ResultVO success() {
        ResultVO res = new ResultVO();
        res.setCode(0);
        res.setSuccess(Boolean.TRUE);
        res.setMsg("请求成功");
        return res;
    }
    public static ResultVO success(Object data) {
        ResultVO res = success();
        res.setData(data);
        return res;
    }
    public static ResultVO success(Object data, String msg) {
        ResultVO res = success();
        res.setData(data);
        res.setMsg(msg);
        return res;
    }

    public static ResultVO fail(Integer code, String msg) {
        ResultVO res = new ResultVO();
        res.setSuccess(Boolean.FALSE);
        res.setMsg(msg);
        res.setCode(code);
        return res;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
