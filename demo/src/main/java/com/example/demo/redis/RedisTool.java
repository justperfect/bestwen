package com.example.demo.redis;

import jakarta.annotation.Resource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisTool {
    @Resource
    private RedisTemplate redisTemplate;

    public void set(Object key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void set(Object key, Object value, long time) {
        redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
    }

    public Object get(Object key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void add(Object key, Object[] values) {
        redisTemplate.opsForSet().add(key, values);
    }

    public void add(Object key, Set<Object> values) {
        redisTemplate.opsForSet().add(key, values.toArray());
    }
}
