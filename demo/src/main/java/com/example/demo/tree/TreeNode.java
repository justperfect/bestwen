package com.example.demo.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TreeNode {
    private String tid;
    private String code;
    private String name;
    private String pid;
    private List<TreeNode> chilren;

    private String idPath;
    private String codePath;
    private String namePath;

    public TreeNode(){}

    public TreeNode(List<TreeNode> chilren) {
        this.chilren = chilren;
    }

    public String getIdPath() {
        return idPath;
    }

    public void setIdPath(String idPath) {
        this.idPath = idPath;
    }

    public String getCodePath() {
        return codePath;
    }

    public void setCodePath(String codePath) {
        this.codePath = codePath;
    }

    public String getNamePath() {
        return namePath;
    }

    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<TreeNode> getChilren() {
        return chilren;
    }

    public void setChilren(List<TreeNode> chilren) {
        this.chilren = chilren;
    }

    /**
     * 遍历当前树的所有子节点
     * @return
     */
    public List<TreeNode> loopChildren() {
        List<TreeNode> list = new ArrayList<>();
        if (this.chilren == null || this.chilren.size() <= 0) {
            return list;
        }
        list.addAll(this.chilren);
        this.chilren.forEach(a->list.addAll(a.loopChildren()));
        return list;
    }
    /**
     * 遍历当前树 包含自己哦
     * @return
     */
    public List<TreeNode> loop() {
        List<TreeNode> list = new ArrayList<>();
        list.add(this);
        list.addAll(loopChildren());
        return list;
    }
}
