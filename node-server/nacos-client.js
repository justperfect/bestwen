// const nacosClient = require('nacosClient');
//
// async function main() {
//     // 创建NacosConfigClient实例
//     const client = nacosClient.createNacosConfigClient({
//         namespace: 'namespace-id', // 如果不需要可以不设置
//         serverAddr: '127.0.0.1:8848', // Nacos服务器地址
//         group: 'DEFAULT_GROUP', // 配置分组，默认为DEFAULT_GROUP
//         dataId: 'example.properties', // 配置文件的dataId
//     });
//
//     // 监听配置信息变化
//     client.listen(data => {
//         console.log('配置已更新:', data);
//     });
//
//     // 获取配置信息
//     const config = await client.getCurrentConfig();
//     console.log('当前配置:', config);
// }
//
// main().catch(err => {
//     console.error('发生错误:', err);
// });


const nacosClient = require('nacos')

const client = new nacosClient.NacosNamingClient({
    serverList:['127.0.0.1:8848'],
    namespace:'gaozw',
    logger:console
})
client.ready()
client.registerInstance('gaozw-nodejs-server', {
    ip:'127.0.0.1',
    port:6060,
    weight:1,
    enable:true,
    healthy:true,
    enabled:true,
    metadata:{
        "nacos.healthcheck.type":"HTTP",
        "nacos.healthcheck.url":"/health",
        "nacos.healthcheck.interval":5,
        "nacos.healthcheck.timeout":3
    }
})
console.log("啦啦啦啦啦")
//
//
// // endpoint?: string;          // 寻址模式下的对端 host
// // serverPort?: number;        // 对端端口
// // namespace?: string;         // 阿里云的 namespace
// // accessKey?: string;         // 阿里云的 accessKey
// // secretKey?: string;         // 阿里云的 secretKey
// // httpclient?: any;           // http 请求客户端，默认为 urllib
// // httpAgent?: any;            // httpAgent
// // appName?: string;           // 应用名，可选
// // ssl?: boolean;              // 是否为 https 请求
// // refreshInterval?: number;   // 重新拉取地址列表的间隔时间
// // contextPath?: string;       // 请求的 contextPath
// // clusterName?: string;       // 请求的 path
// // requestTimeout?: number;    // 请求超时时间
// // defaultEncoding?: string;   // 请求编码
// // serverAddr?: string;        // 用于直连，包含端口
// // unit?: string;              // 内部单元化用
// // nameServerAddr?: string;    // 老的兼容参数，逐步废弃，同 endpoint
// // username?: string;          // 认证的用户名
// // password?: string;          // 认证的密码
// // cacheDir?: string;          // 缓存文件的路径
// // identityKey?: string;       // Identity Key
// // identityValue?: string;     // Identity Value
// // endpointQueryParams?: string; // endPoint 查询参数 e.g: param_1=1&param_2=2
// // decodeRes?: (res: any, method?: string, encoding?: string) => any
// const config = new nacosClient.NacosConfigClient({
//     endpoint:'127.0.0.1',
//     serverPort:8848,
//     serverAddr:'http://127.0.0.1:8848/nacos',
//     decodeRes:(res, method, encoding) => {
//         console.log(res, method, encoding)
//     }
// })
//
