

// const { Client } = require('elasticsearch');
// const fk = require('faker')
// // 创建Elasticsearch客户端实例
// const client = new Client({
//     node: 'http://localhost:9200', // 替换为您的Elasticsearch节点地址
// });

// async function createIndex() {
//     // 创建索引请求
//     const response = await client.indices.create({
//         index: 'assets', // 您想要创建的索引名称
//         body: {
//             mappings: {
//                 properties: {
//                     // 定义文档字段的映射
//                     tid: {
//                         type: 'keyword',
//                     },
//                     assetName: {
//                         type: 'text',
//                     },
//                     categoryIds: {
//                         type: 'keyword',
//                     }
//
//                 },
//             },
//         },
//     });
//
//     console.log(response);
// }
//
// // 调用函数创建索引
// createIndex().catch(console.error);
// // 插入数据
// const indexData = async () => {
//     try {
//         const response = await client.index({
//             index: 'assets', // 你要插入数据的索引名
//             id: '1', // 可选的ID，如果不指定，Elasticsearch将自动生成
//             body: { // 要插入的数据
//                 tid: '1',
//                 assetName: '测试资产001',
//                 categoryIds: ['srcDepart', '1000', '1100', '1106']
//             }
//         });
//         console.log(response);
//     } catch (error) {
//         console.error('Indexing error:', error);
//     }
// };
//
// indexData().then(r => console.error(r));
// const es = require('elasticsearch')
// const faker = require('faker')
// const firstName =  faker['name'].firstName();
// console.log(firstName)
// const esClient = new es.Client({
//     host:'127.0.0.1:9200',
//     log: 'error'
// })
// console.log(esClient)

// 准备要批量插入的数据
// const actions = [
// ];
//
// for (let i = 30000 ; i < 40000; i++) {
//     let thisId = (100000 + i);
//     actions.push({ index: { _index: 'assets', _id: thisId } });
//     actions.push({
//         tid: thisId,
//         assetName: '测试资产' + thisId,
//         categoryIds: [
//             'srcDepart'
//             , '10000'
//             , fk.datatype.number({ min: 0, max: 10000 }) + 300000 + ''
//             , fk.datatype.number({ min: 0, max: 10000 }) + 300000 + ''
//         ]
//     });
// }
// // 执行批量插入
// client.bulk({ body: actions }, (err, response) => {
//     if (err) {
//         console.error('Failed to bulk index data:', err);
//     } else {
//         console.log('Bulk index data successful:', response);
//     }
// });
